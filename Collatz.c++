// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <vector>

#include "Collatz.h"
#define MAX_LIMIT 1000000

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval helper function
int helper_collatz_eval(long int num,int cache[]) {
    if (num == 1) {
        return 1;
    }
    else if (num < MAX_LIMIT && cache[num] != 0) {
        return cache[num]+1;
    }
    else if (num % 2 == 0) {
        if (num < MAX_LIMIT) //if it fits in the cache
            return (cache[num] = helper_collatz_eval(num/2,cache))+1; //increments the step by 1 and sets cache value
        else
            return helper_collatz_eval(num/2,cache)+1;
    }
    else {
        if (num < MAX_LIMIT)
            return (cache[num] = helper_collatz_eval(3*num+1,cache))+1;
        else
            return helper_collatz_eval(3*num+1,cache)+1;
    }
}
// ------------
// collatz_eval
// ------------

int cache[MAX_LIMIT] = {};
int collatz_eval (int i, int j) {
    assert(i > 0 && j > 0);
    assert(i < MAX_LIMIT && j < MAX_LIMIT);
    int max_count = 0;
    int min = i < j ? i : j;
    int max = i > j ? i : j;
    if (min < ((max / 2) + 1))   // cuts down the numbers needed to iterate through
        min = (max / 2) + 1;
    for (long int num = min; num <= max; ++num) {
        int count = helper_collatz_eval(num,cache);
        if (count > max_count)
            max_count = count;
    }
    assert(max_count >= 1);
    return max_count;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
